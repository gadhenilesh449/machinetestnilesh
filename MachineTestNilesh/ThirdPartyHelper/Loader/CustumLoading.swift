//
//  CustumLoading.swift
//  boozelee
//
//  Created by Sharvari-Amol on 22/08/19.
//  Copyright © 2019 AshwiniChoudhar. All rights reserved.
//

import UIKit

class CustumLoader: UIView {

    static let instance = CustumLoader()
    
    
    lazy var transparentView : UIView = {
        let transparentView = UIView(frame: UIScreen.main.bounds)
        transparentView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        transparentView.isUserInteractionEnabled = false
        return transparentView
    }()

    lazy var gifImage : UIImageView = {
        let gifImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        gifImage.contentMode = .scaleAspectFit
        gifImage.backgroundColor = UIColor.hexStringToUIColor(hex: "#FCEC31")
        gifImage.layer.cornerRadius = 5
        gifImage.layer.masksToBounds = false
        gifImage.clipsToBounds = true
        gifImage.center = transparentView.center
        gifImage.isUserInteractionEnabled = false
        gifImage.loadGif(name: "loader")
        return gifImage
    }()
    
    func showLoader(){
        self.addSubview(transparentView)
        self.transparentView.addSubview(gifImage)
        self.transparentView.bringSubviewToFront(self.gifImage)
        UIApplication.shared.keyWindow?.addSubview(transparentView)
    }
    
    func hideLoader(){
        self.transparentView.removeFromSuperview()
    }
    
}
