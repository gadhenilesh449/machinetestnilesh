//
//  AddJournalViewController.swift
//  MachineTestNilesh
//
//  Created by Codepix on 15/06/20.
//  Copyright © 2020 Nilesh Gadhe. All rights reserved.
//

import UIKit
import DKImagePickerController
import DatePickerDialog
import Alamofire

class AddJournalViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var buttonSelect: UIButton!
    @IBOutlet weak var buttonSelectImage: UIButton!
    @IBOutlet weak var buttonPostActivity: UIButton!
    
    @IBOutlet weak var locationContainerView: UIView!
    @IBOutlet weak var dateContainerView: UIView!
    @IBOutlet weak var experinceContainerView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var txtFieldLocation: UITextField!
    @IBOutlet weak var txtFieldDate: UITextField!

    @IBOutlet weak var txtViewExperience: UITextView!
    
    @IBOutlet weak var tabBar: UITabBar!
    //image
    var pickerController: DKImagePickerController!
    var assets: [DKAsset]?
    var postImages = [MyAdsImageModel]()
    var imgArray = [UIImage]();
    let imagePicker = UIImagePickerController()
    
    //PostDate
    var postDate = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Add Journal"
        self.navigationController?.isNavigationBarHidden = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        addRightBarBtn()
        setUpView()
        
        
    }
    
    
    func addRightBarBtn(){
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "ic_chat"), for: .normal)
        button1.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
    
        let barView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        barView.addSubview(button1)
        
        let item1 = UIBarButtonItem(customView: barView)
    
        self.navigationItem.rightBarButtonItem = item1
    }
    
    func setUpView(){
        
        setButton(button: buttonSelect, cronerRadius: buttonSelect.frame.height/2)
        setButton(button: buttonSelectImage, cronerRadius: 5)
        setButton(button: buttonPostActivity, cronerRadius: buttonPostActivity.frame.height/2)
        
        setCared(view: locationContainerView, cronerRadius: locationContainerView.frame.height/2)
        setCared(view: dateContainerView, cronerRadius: dateContainerView.frame.height/2)
        setCared(view: experinceContainerView, cronerRadius: 5)
        
    }

    @IBAction func buttonSelectImageTapped(_ sender: Any) {
        showImagePicker()
    }
    
    func showImagePicker() {
        var pickerController: DKImagePickerController!
        var assets: [DKAsset]?
        
        if pickerController == nil {
            pickerController = DKImagePickerController()
        }
        pickerController.sourceType = .both
        pickerController.maxSelectableCount = 5
        pickerController.showsCancelButton = true
        //pickerController. = self.assets!
        pickerController.navigationBar.isTranslucent = false
        pickerController.navigationBar.barTintColor = UIColor.black
        pickerController.navigationBar.tintColor = .white
        pickerController.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        
        pickerController.didCancel = { ()
            print("didCancel")
        }
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            print("didSelectAssets")
            
            self.postImages.removeAll();
            self.imgArray.removeAll();
            for asset in assets {
                
                asset.fetchOriginalImage(completeBlock: { (image, info) in
                    if let img = image {
                        // let fixOrientationImage=img.fixOrientation()
                        var imageToPost = UIImage()
                        if let imageData = img.pngData() {
                            let bytes = imageData.count
                            let kB = Double(bytes) / 1000.0 // Note the difference
                            let KB = Double(bytes) / 1024.0 // Note the difference
                            let MB = Double(KB/1024)
                            let fileSize = MB
                            if fileSize > 1.0{
                                
                                if let convertedImageData = img.jpegData(compressionQuality: 50){
                                    
                                    let convertedImage = UIImage(data: convertedImageData)
                                    print(imageData.count)
                                    imageToPost = convertedImage!
                                    
                                }
                                
                            } else{
                                imageToPost = img
                            }
                        } else{
                            imageToPost = img
                        }
                        
                        let postSingleImage = MyAdsImageModel()
                        postSingleImage.uiImage = imageToPost
                        self.postImages.append(postSingleImage);
                        self.imgArray.append(imageToPost)
                        self.collectionView?.reloadData()
                        print("image added to array")
                        
                    }
                    if self.imgArray.count == assets.count{
                        
                    }
                })
                
            }
            
        }
        
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            pickerController.modalPresentationStyle = .formSheet
        }
        
        self.present(pickerController, animated: true) {}
    }
    
    
    
    
    @IBAction func buttonPostActivityTapped(_ sender: Any) {
        uploadFile()
    }
    
    
    
    @IBAction func buttonSelectDateTapped(_ sender: Any) {
        datePickerOpen()
        
    }
    
    func datePickerOpen(){
        DatePickerDialog().show("Select Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if(date != nil){
                let formatter = DateFormatter()
                formatter.dateFormat = "YYYY-MM-dd"
                let dateString = formatter.string(from: date!)
                self.postDate = dateString
                let formatter1 = DateFormatter()
                formatter1.dateFormat = "dd MMM yyyy"
                let dispayDate = formatter1.string(from: date!)
                
                let today:Bool = Calendar.current.isDateInToday(date!)
                print(today);
                if(today) {
                    self.txtFieldDate.text = dispayDate;
                }
                else if (date?.timeIntervalSinceNow.sign == .minus) {
                    
                    let myAlert = UIAlertController(title: "Alert", message: "Please select correct date" ,preferredStyle: UIAlertController.Style.alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
                    
                    myAlert.addAction(okAction)
                    self.present( myAlert, animated: true, completion: nil )
                }else {
                    self.txtFieldDate.text = dispayDate;
                }
            }
            
        }
    }
    
    
    func uploadFile(){
        
        let parameters = [
             "latitude":"18.5204",
             "longitude":"73.8567",
             "country":"India",
             "state":"Maharastra",
             "city":"Pune",
             "address":"Pune",
             "start_date":txtFieldDate.text!,
             "experience":txtViewExperience.text!,
             "hashtags":"All",
             "type":"Journal",
             "vid":"1"
        ]
        
        
        let urlString = "http://stagging.omsoftware.org/public/api/add-journal"
        CustumLoader.instance.showLoader()
        let arr = self.imgArray
        uploadAttachmentMultipart(url : urlString,parameter: parameters, attachmentArray: imgArray)
        
    }
    
    
    func uploadAttachmentMultipart(url : String, parameter : Dictionary<String,String> ,attachmentArray:[UIImage]){
        
        let header = [
            "Authorization" : Urls().authrizationKey
        ]
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            for (key, value) in parameter {
                multipartFormData.append((value.data(using: .utf8))!, withName: key)
            }
            
            for image in attachmentArray{
                let uIvalue = image as! UIImage
                // let image = UIImage.init(named: key)
                let imgData = uIvalue.jpegData(compressionQuality: 50)
                
                multipartFormData.append(imgData!, withName: "img[]",fileName: "file.jpg", mimeType: "image/jpg")
            }
        },
        usingThreshold:UInt64.init(),
        to:String(format:url),
        method:.post,
        headers:header,
        encodingCompletion: { encodingResult in
            switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                        // Show uploading progress
                        
                    })
                    
                    upload.responseJSON { response in
                        // SVProgressHUD.showSuccess(withStatus: "Uploading Complited")
                        CustumLoader.instance.hideLoader()
                        self.alertView(message: "Image uploaded successfully")
                        DispatchQueue.main.async(execute: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        //print(response.result.value)
                        
                }
                case .failure(let encodingError):
                    //SVProgressHUD.showError(withStatus: "Fail to upload attachments.")
                    print("Fail to upload attachments.")
                    print(encodingError)
                }
        })
  
    }

}


extension AddJournalViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.postImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell?
        var imageView: UIImageView?
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellImage", for: indexPath)
        imageView = cell?.contentView.viewWithTag(1) as? UIImageView
        
        
        if(self.postImages[indexPath.row].uiImage != nil)
        {
            imageView?.image = self.postImages[indexPath.row].uiImage!
        }
        
        imageView?.layer.cornerRadius = 5.0;
        imageView?.clipsToBounds = true;
        
        return cell!
    }
    
}




