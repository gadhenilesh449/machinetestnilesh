//
//  ViewJournalViewController.swift
//  MachineTestNilesh
//
//  Created by Codepix on 15/06/20.
//  Copyright © 2020 Nilesh Gadhe. All rights reserved.
//

import UIKit
import Alamofire

class ViewJournalViewController: UIViewController {
    
    
    @IBOutlet weak var buttonAddNewEntry: UIButton!
    @IBOutlet weak var buttonWhere: UIButton!
    @IBOutlet weak var buttonWhat: UIButton!
    @IBOutlet weak var buttonMyProfile: UIButton!
    @IBOutlet weak var buttonMyActivities: UIButton!
    @IBOutlet weak var tabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "View Journal"
        self.navigationController?.isNavigationBarHidden = false
        
        addRightBarBtn()
        addLeftBarBtn()
        setUpView()
        apiCallViewJournal()
    }
    
    func addRightBarBtn(){
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "ic_chat"), for: .normal)
        button1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        let barView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        barView.addSubview(button1)
        
        let item1 = UIBarButtonItem(customView: barView)
        
        self.navigationItem.rightBarButtonItem = item1
    }
    
    func addLeftBarBtn(){
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "ic_account"), for: .normal)
        button1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        let barView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        barView.addSubview(button1)
        
        let item1 = UIBarButtonItem(customView: barView)
        
        self.navigationItem.leftBarButtonItem = item1
    }
    
    func setUpView(){
        setButton(button: buttonAddNewEntry, cronerRadius: buttonAddNewEntry.frame.height/2)
        setButton(button: buttonWhere, cronerRadius: buttonWhere.frame.height/2)
        setButton(button: buttonWhat, cronerRadius: buttonWhat.frame.height/2)
        setButton(button: buttonMyProfile, cronerRadius: buttonMyProfile.frame.height/2)
        setButton(button: buttonMyActivities, cronerRadius: buttonMyActivities.frame.height/2)
        
        tabBar.layer.cornerRadius = 5
    }

    
    @IBAction func addNewEntryButtonTapped(_ sender: Any) {
        
        let viewController: AddJournalViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddJournalViewController") as! AddJournalViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func apiCallViewJournal(){
        //addAddress
        if(isInternetAvailable()) {
            
            
            CustumLoader.instance.showLoader()
            let headers = [
                "Authorization" : Urls().authrizationKey
            ]
            
            let params = [
                        "page":"1",
                        "limit":"10",
                        "what":"Journal",
                        "distance":"1000",
                        "country":"India"
                ] as [String : Any]
            
            let strUrl = Urls().viewJuornal
            
            Alamofire.request(strUrl, method: .post, parameters: params, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                
                
                CustumLoader.instance.hideLoader()
                
                switch(response.result) {
                    
                case.success(let data):
                    print("success",data)
                    let dict = response.result.value as! Dictionary<String,Any>
                    let status = dict["status"] as? Bool ?? false
                    let msj = dict["message"] as? String ?? ""
                    
                    if status == true{
                        self.alertView(message: msj, title: "")
                    } else{
                        self.alertView(message: msj, title: "")
                    }
                    
                case.failure(let error):
                    print("Not Success",error)
                    
                }
                
            }
            
        } else{
            
            //self.alertView(message: "Internet connection not available! Please check your internet connectivity and try again.", title: "")
            self.alertView(message: "Internet connection not available! Please check your internet connectivity and try again.")
        }
        
    }
    
    
    
    
}
